package mandelbrot;

import java.awt.Color;
import java.awt.image.*;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;


public class Mandelbrot 
{

	public static void main(String[] args) 
	{
		int width = 3840*2; 
		int height = 2160*2;
		int max = 1000;
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_4BYTE_ABGR_PRE);
		
		/*Color White = new Color(255, 255, 255); // Color white
		int white = White.getRGB();*/
		
		int[] colors = new int[max]; //An array of size max, for each possible number of iterations. 
		colors[0] = Color.red.getRGB();
		for(int i = 0; i < max; i++)
		{
			//colors[i] = new Color(i%255, i/5 % 255, i/8 % 255).getRGB();
			colors[i] = Color.HSBtoRGB((float)Math.log10(i), (float) 1.0, i/(i+8f));
		}
		
		Color Black = new Color(0, 0, 0); // Color white
		int black = Black.getRGB();
		
		//Iterate through all rows of the image
		for(int row = 0; row < width; row++)
		{
			//Iterate through all columns of the image. Effectively traverse the entire image.
			for(int col = 0; col < height; col++)
			{
				double c_re = (row-width/2) * (4.0/width); //center of image is 0 for c_re/c_im.
				double c_im = (col-height/2) * (4.0/width); //4/width ensures entire image is 4 units.
				//System.out.println(c_re + " + " + c_im + "i");
				double a = 0; //a and b represent complex number z in equation fc(z) = z^2 + c; a+bi
				double b = 0;
				int iteration = 0; //A counter to keep track of iterations
				while(a*a+b*b <= 4 && iteration < max) //magnitude of z <= 2^2
				{
					double a_new = (a*a) - (b*b) + c_re;
					b = 2*a*b + c_im;
					a = a_new;
					iteration++;
				}
				if(iteration < max)//magnitude became greater than 2
				{ 
					image.setRGB(row, col, colors[iteration]);
				}
				else
				{
					image.setRGB(row, col, black);
				}
			}
			
		}
		try {
			ImageIO.write(image, "png", new File("mandelbrot_color.png"));
		} catch (IOException e) {
			System.out.println("Image Creation Failed");
		}
	}

}

